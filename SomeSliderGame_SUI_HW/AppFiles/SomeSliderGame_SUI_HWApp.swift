//
//  SomeSliderGame_SUI_HWApp.swift
//  SomeSliderGame_SUI_HW
//
//  Created by Maksim on 24.12.2023.
//

import SwiftUI

@main
struct SomeSliderGame_SUI_HWApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
