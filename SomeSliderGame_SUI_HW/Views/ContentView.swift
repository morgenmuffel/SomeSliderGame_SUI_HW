//
//  ContentView.swift
//  SomeSliderGame_SUI_HW
//
//  Created by Maksim on 24.12.2023.
//

import SwiftUI

struct ContentView: View {
    
    @State private var randomNumber = Int.random(in: 1...100)
    @State var currentValue = 50.0
    @State var alertPresented = false
    
    var body: some View {
        
        VStack {
            Text("Подвинте слайдер, как можно ближе к: \(randomNumber)")
            
            HStack {
                Text("0")
                SliderView(sliderValue: $currentValue, alpha: computeScore())
                    .onChange(of: currentValue) { newValue in
                    }
                Text("100")
            }
            .padding()
         
            Button {
                alertPresented = true
                }
                label: {
                    Text("Проверь меня!")
            }
            .padding(.bottom, 20)
            .alert("Отлично!", isPresented: $alertPresented)
                {
                Button("Ok", action: {})
                } message: {
                 Text ("Точность: \(computeScore()) %")
                }
            
            Button {
                randomNumber = Int.random(in: 1...100)
            } label: {
               Text("Начать заново")
            }
        }
    }
    
    private func computeScore() -> Int {
        let targetValue = randomNumber
        let difference = abs(targetValue - lround(currentValue))
        return 100 - difference
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
